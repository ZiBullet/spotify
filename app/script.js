/* -------------------------------------------------------------------------- */
/*                                   Imports                                  */
/* -------------------------------------------------------------------------- */
import { host, isPremium } from "./modules/host.js";
import { openSearch, closeSearch } from "./modules/search.js"
import { containers, getRandomNum, createSongCol, createRndCol, createPlayer } from "./modules/functions.js"
import { createHeader, createNav, showNavSongs } from "./modules/createHeadNav.js";
import { createModal } from './modules/modal.js'
/* -------------------------------------------------------------------------- */
/*                                  Load Page                                 */
/* -------------------------------------------------------------------------- */
const slideContainer = document.querySelector('.main-screen__one-swiper-wrapper')
createNav()
createHeader()
let searchBar = document.querySelector('#search')
let searchDrop = document.querySelector('.header-search__drop')
isPremium()
createModal()
/* -------------------------------------------------------------------------- */
/*                                   Swiper                                   */
/* -------------------------------------------------------------------------- */
const scrOneSwiper = new Swiper('.main-screen__one-swiper', {
    navigation: {
        nextEl: '.main-screen__one-swiper-btn__next',
        prevEl: '.main-screen__one-swiper-btn__prev'
    },
    pagination: {
        el: '.main-screen__one-swiper-pag',
        clickable: true
    },
    effect: 'fade',
    fadeEffect: {
        crossFade: true
    },
})

/* -------------------------------------------------------------------------- */
/*                          Script of page / starts                           */
/* -------------------------------------------------------------------------- */
// ____________________Search______________________
searchBar.onblur = () => {
    closeSearch(searchDrop)
}
function createSlide(tracks, slideCont) {
    slideCont.innerHTML = ''
    for (let num of getRandomNum(tracks.length, 4)) {
        let slide = document.createElement('div')
        let slideLeft = document.createElement('div')
        let artist = document.createElement('h2')
        let musicInfo = document.createElement('div')
        let musicName = document.createElement('h4')
        let musicDuration = document.createElement('span')
        let slideRight = document.createElement('div')
        let musicPoster = document.createElement('img')

        slide.classList.add('main-screen__one-swiper-slide')
        slide.classList.add('swiper-slide')
        slideLeft.classList.add('main-screen__one-swiper-slide-music__info')
        artist.innerHTML = tracks[num].artist
        musicInfo.classList.add('main-screen__one-swiper-slide-music__dur__name')
        musicName.innerHTML = tracks[num].song.name
        musicDuration.innerHTML = tracks[num].song.duration
        slideRight.classList.add('main-screen__one-swiper-slide-music__wrapper')
        musicPoster.src = tracks[num].song.poster
        musicPoster.setAttribute('loading', 'lazy')

        slideRight.append(musicPoster)
        musicInfo.append(musicName, musicDuration)
        slideLeft.append(artist, musicInfo)
        slide.append(slideLeft, slideRight)
        slideCont.append(slide)
    }
}
    axios.get(`${host}/tracks`) 
    .then(res => {
    if (res.status === 200 || res.status === 201) {
            searchBar.onfocus = () => {
                openSearch(searchBar, searchDrop, res.data)
            }
            let favorites = res.data.filter(item => item.isFavorite)
            showNavSongs(favorites)
            createSlide(res.data, slideContainer)
            if (innerWidth < 768) {
                createSongCol(favorites.slice(0, 5), containers.lkdSongCont)
                createRndCol(res.data, containers.rndSongCont, 5)
            } else {
                createSongCol(favorites.slice(0, 10), containers.lkdSongCont)
                createRndCol(res.data, containers.rndSongCont, 10)
            }
        }
    })
createPlayer()