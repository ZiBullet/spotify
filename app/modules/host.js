export const host = 'http://localhost:3001'
export function isPremium() {
    let status = JSON.parse(localStorage.getItem('user')) || {userName: 'Alex Adams'}
    let switcher = document.querySelector('.switcher')
    if (status.isPremium !== undefined) {
        switcher.href = '/app/css/premium.css'
    } else {
        switcher.href = '/app/css/unpremium.css'
    }
}