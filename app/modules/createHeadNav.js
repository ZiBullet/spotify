const body = document.body
import { host } from './host.js'
import { createSongCol } from './functions.js'
export function createHeader() {
    let user = JSON.parse(localStorage.getItem('user')) || { userName: 'Alex Adams' }
    let header = document.createElement('header')
    header.classList.add('header')
    header.innerHTML = `
    <div class="header__container">
    <div class="header-heading">
        <form>
            <label for="search">
                <input type="text" name="search" id="search" placeholder="Search by name and artist">
            </label>
            <div class="header-search__drop"></div>
        </form>
        <div class="header-user__info">
            <div class="header-user__name__block">
                <a href="index.html">
                    <img src="/app/assets/Icons/logo.svg" alt="">
                </a>
                <h2 class="header-user__name">${user.userName}</h2>
            </div>
            <span class="header-user__status">${user.isPremium !== undefined ? 'Premium User' : 'Free User'}</span>
        </div>
        <div class="header-menu__box">
            <div class="menu-btn">
                <span></span>
                <span></span>
            </div>
            <img src="/app/assets/Icons/search.svg" alt="">
        </div>
    </div>
</div>
    `
    body.prepend(header)
    let menu = document.querySelector('.menu-btn')
    let nav = document.querySelector('aside')
    let lines = document.querySelectorAll('.menu-btn span')
    menu.onclick = () => {
        if (nav.classList.contains('opened')) {
            closeNav()
        } else {
            openNav()
        }
    }
    function openNav() {
        nav.classList.add('opened')
        lines[0].style.transform = ('rotate(45deg)')
        lines[1].style.transform = ('rotate(-45deg) translate(6px, -6px)')
        nav.style.display = 'flex'
        body.style.overflow = 'hidden'
        setTimeout(() => {
            nav.style.opacity = '1'
            nav.style.top = '20%'
        }, 200);
    }
    function closeNav() {
        nav.classList.remove('opened')
        nav.style.top = '0%'
        nav.style.opacity = '0'
        setTimeout(() => {
            lines[0].style.transform = ('rotate(0)')
            lines[1].style.transform = ('rotate(0) translate(0)')
            nav.style.display = 'none'
            body.style.overflow = 'auto'
        }, 200);
    }
}
export function createNav() {
    let mainConainer = document.querySelector('.main__container')
    let nav = document.createElement('aside')
    nav.classList.add('nav')
    nav.innerHTML = `
    <nav class="nav-content">
    <a href="/app/index.html" class="nav-logo">
        <img src="/app/assets/Icons/logo.svg" alt="logo">
    </a>
    <div class="nav-playlists__collection">
        <div class="nav-playlist">
            <h2 class="nav-playlist__title">Navigation</h2>
            <a href="/app/index.html" class="nav-playlist__item">Homepage</a>
            <a href="/app/pages/playlists.html" class="nav-playlist__item">Playlists</a>
            <a href="/app/pages/settings.html" class="nav-playlist__item">Settings</a>
        </div>
        <div class="nav-playlist">
            <h2 class="nav-playlist__title">Playlists</h2>
            <button class="nav-playlist__item">Liked songs <img src="/app/assets/Icons/love.svg" alt=""></button>
            <div class="nav-user__playlist__items"></div>
        </div>
        <div class="nav-playlist">
            <h2 class="nav-playlist__title">Liked songs</h2>
            <div class="nav-user__liked__songs"></div>
        </div>
    </div>
    `
    body.append(nav)
    let navBtns = document.querySelectorAll('.nav-playlist__item')
    navBtns.forEach(btn => {
        btn.onclick = () => {
            navBtns.forEach(remover => {
                remover.classList.remove('active')
            })
            btn.classList.add('active')
        }
    })
    let playlistsNavCol = document.querySelector('.nav-user__playlist__items')
    function showPlaylists(data) {
        playlistsNavCol.innerHTML = ''
        for (let datum of data) {
            let playlistsBtns = document.createElement('button')
            playlistsBtns.classList.add('nav-playlist__item')
            playlistsBtns.innerHTML = datum.name
            playlistsNavCol.append(playlistsBtns)
            playlistsBtns.onclick = () => {
                createPage(datum, mainConainer)
            }
        }
    }
    axios.get(`${host}/playlists`)
        .then(res => {
            if (res.status === 200 || res.status === 201) {
                showPlaylists(res.data)
            }
        })
}

function createPage(datum, cont) {
    cont.innerHTML = ''
    cont.innerHTML = `
        <h2 class="container-title">${datum.name}</h2>
        <span class="song__amount">${datum.tracks.length} song(s)</span>
        <div class="container-content"></div>
    `
    let plCont = document.querySelector('.container-content')
    createSongCol(datum.tracks, plCont)
}
export function showNavSongs(data) {
    let lkdSongCont = document.querySelector('.nav-user__liked__songs')
    for (let datum of data) {
        let songBox = document.createElement('div')
        let songName = document.createElement('h4')
        let songDuration = document.createElement('span')
        songBox.classList.add('nav-playlist__song')
        songName.classList.add('nav-song__name')
        songName.innerHTML = datum.song.name
        songDuration.classList.add('nav-song__duration')
        songDuration.innerHTML = datum.song.duration
        songBox.append(songName, songDuration)
        lkdSongCont.append(songBox)
    }
}
