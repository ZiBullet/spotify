import { host } from './host.js'

export function createModal() {
    let modal = document.createElement('div')
    let modalBg = document.createElement('div')

    modal.classList.add('modal')
    modalBg.classList.add('modal__bg')
    modalBg.classList.add('modalCloser')
    modal.innerHTML = `
    <div class="closer">
        <img class="modalCloser" src="/app/assets/Icons/close.svg" alt="closer">
    </div>
    <h2 class="modal-title">Add to playlist</h2>
    <div class="modal-playlists-container"></div>
    <div class="modal-bottom">
        <button class="showOffer">Create new playlist</button>
        <form name="playlistCreator">
            <input type="text" name="name" placeholder="Name of playlist">
            <button>Save</button>
        </form>
    </div> 
    `
    document.body.prepend(modal, modalBg)

    
    let form = document.forms.playlistCreator
    let inputs = form.querySelectorAll('input')
    let closers = document.querySelectorAll('.modalCloser')

    closers.forEach(closer => {
        closer.onclick = () => {
            closeModal(modal, modalBg)
        }
    })
    form.onsubmit = () => {
        let errs = []
        inputs.forEach(input => {
            if (input.value.length === 0) {
                errs.push('error')
            }
        })
        if (errs.length === 0) {
            submit()
        }
    }
    let showOffer = document.querySelector('.showOffer')
    showOffer.onclick = () => {
        showOffer.style.opacity = '0'
        form.style.display = 'flex'
        setTimeout(() => {
            showOffer.style.display = 'none'
            form.style.opacity = '1'
        }, 200);
    }
    function submit() {
        let playlist = {
            tracks: []
        }
        let fm = new FormData(form)
        fm.forEach((value, key) => {
            playlist[key] = value
        })
        axios.post(`${host}/playlists`, playlist)
    }
    function closeModal(modal, modalBg) {
        modal.style.opacity = '0'
        modalBg.style.opacity = '0'
        setTimeout(() => {
            modal.style.display = 'none'
            modalBg.style.display = 'none'
            document.body.style.overflow = 'auto'
        }, 200);
    }
    
}

export function openModal(modal, modalBg, track) {
    document.body.style.overflow = 'hidden'
    modal.style.display = 'flex'
    modalBg.style.display = 'block'
    setTimeout(() => {
        modal.style.opacity = '1'
        modalBg.style.opacity = '1'
    }, 200);
    let playlistCont = document.querySelector('.modal-playlists-container')
    axios.get(`${host}/playlists`)
    .then(res => {
        if (res.status === 200 || res.status === 201) {
            createPlaylist(res.data, playlistCont, track)
        }
    })
    function createPlaylist(data, cont, track) {
        cont.innerHTML = ''
        for (let datum of data) {
            let playlistRow = document.createElement('div')
            let playlistName = document.createElement('h4')
            let playlistLength = document.createElement('span')

            playlistRow.classList.add('modal-playlist')
            playlistName.classList.add('modal-playlist__name')
            playlistName.innerHTML = datum.name
            playlistLength.classList.add('modal-playlist__amount')
            playlistLength.innerHTML = `${datum.tracks.length} song(s)`

            playlistRow.append(playlistName, playlistLength)
            cont.append(playlistRow)
            playlistRow.onclick = () => {
                if (!datum.tracks.includes(track)) {
                    datum.tracks.push(track)
                    axios.patch(`${host}/playlists/${datum.id}`, datum)
                }
            }
        }
    }
}
