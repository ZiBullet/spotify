import { host } from "./host.js"
import { openModal } from "./modal.js"

export const containers = {
    rndSongCont: document.querySelector('.random-container'),
    lkdSongCont: document.querySelector('.liked-container'),
    lstLstndSongCont: document.querySelector('.last-container')
}
export function getRandomNum(limit, amount) {
    return _.shuffle(_.range(0, limit)).slice(0, amount)
}
function dataPatch(url, direction, item, curItem) {
    axios.patch(`${url}/${direction}/${item}`, curItem)
}
export function createSongCol(tracks, cont) {
    let user = JSON.parse(localStorage.getItem('user')) || {userName: 'Alex Adams'}
    let modal = document.querySelector('.modal')
    let modalBg = document.querySelector('.modal__bg')
    let audio = document.querySelector('audio')
    cont.innerHTML = ''
    if (tracks.length === 0 && !cont.classList.contains('extra-class')) {
        cont.innerHTML = `<p class="empty-cont">No songs here <img src="/app/assets/Icons/cry.svg" ></p>"`
    }
    for (let track of tracks) {
        let musicBox = document.createElement('div')
        let musicBoxLeft = document.createElement('div')
        let songNum = document.createElement('span')
        let songImg = document.createElement('img')
        let musicBoxAristInfo = document.createElement('div')
        let trackName = document.createElement('h4')
        let artistName = document.createElement('span')
        let musicBoxRight = document.createElement('div')
        let likeIcon = document.createElement('img')
        let musicDuration = document.createElement('span')
        let menuBox = document.createElement('div')
        let menuDrop = document.createElement('div')
        let dropBtnLike = document.createElement('button')
        let dropBtnAdd = document.createElement('button')
        let dropBtnListen = document.createElement('button')
        let menuIcon = document.createElement('div')
        for (let i = 1; i <= 3; i++) {
            let dot = document.createElement('span')
            menuIcon.append(dot)
        }
        musicBox.classList.add('main-screen__two-container-music__box')
        musicBoxLeft.classList.add('music__box-left')
        songNum.innerHTML = tracks.indexOf(track) + 1
        songImg.src = track.song.poster
        songImg.classList.add('music__box-left-track__poster')
        musicBoxAristInfo.classList.add('music__box-artist-info')
        trackName.innerHTML = track.song.name
        artistName.innerHTML = track.artist
        musicBoxRight.classList.add('music__box-right')
        if (track.isFavorite && user.isPremium === undefined) {
            likeIcon.classList.add('active')
            likeIcon.src = '/app/assets/Icons/active-love.svg'
        } else if (track.isFavorite) {
            likeIcon.classList.add('active')
            likeIcon.src = '/app/assets/Icons/love-active-pr.svg'
        } else {
            likeIcon.classList.remove('active')
            likeIcon.src = '/app/assets/Icons/love.svg'
        }

        musicDuration.classList.add('music__duration')
        musicDuration.innerHTML = track.song.duration
        menuBox.classList.add('menu__box')
        menuDrop.classList.add('menu__drop')
        track.isFavorite ? dropBtnLike.innerHTML = 'Dislike' : dropBtnLike.innerHTML = 'Like';
        dropBtnAdd.innerHTML = 'Add to playlist'
        dropBtnListen.innerHTML = 'Listen now'
        menuIcon.classList.add('menu__icon')

        menuDrop.append(dropBtnLike, dropBtnAdd, dropBtnListen)
        menuBox.append(menuDrop, menuIcon)
        musicBoxRight.append(likeIcon, musicDuration, menuBox)
        musicBoxAristInfo.append(trackName, artistName)
        musicBoxLeft.append(songNum, songImg, musicBoxAristInfo)
        musicBox.append(musicBoxLeft, musicBoxRight)
        cont.append(musicBox)
        dropBtnLike.onclick =
            likeIcon.onclick = () => {
                track.isFavorite ? track.isFavorite = false : track.isFavorite = true
                createSongCol(tracks, cont)
                dataPatch(host, 'tracks', track.id, track)
            }
        dropBtnAdd.onclick = () => {
            openModal(modal, modalBg, track)
        }
        let menuDrops = document.querySelectorAll('.menu__drop')
        let menuBoxes = document.querySelectorAll('.main-screen__two-container-music__box')
        menuIcon.onclick = () => {
            menuBoxes.forEach(box => {
                menuDrops.forEach(drop => {
                    box.classList.remove('clicked')
                    drop.classList.remove('clicked')
                })
            })
            menuBox.classList.add('clicked')
            menuDrop.classList.add('clicked')
        }
        dropBtnListen.onclick = () => {
            if (audio.classList.contains('pending')) {
                pauseMusic(audio, document.querySelector('.play'))
            } else {
                initMusic(track, audio)
                playMusic(audio, document.querySelector('.play'))
            }
        }
    }
}
/* -------------------------------------------------------------------------- */
/*                             Random Music Create                            */
/* -------------------------------------------------------------------------- */
export function createRndCol(tracks, cont, limit) {
    let user = JSON.parse(localStorage.getItem('user')) || {userName: 'Alex Adams'}
    let audio = document.querySelector('audio')
    let order = 1
    cont.innerHTML = ''
    for (let num of getRandomNum(tracks.length, limit)) {
        let songNum = document.createElement('span')
        let musicBox = document.createElement('div')
        let musicBoxLeft = document.createElement('div')
        let songImg = document.createElement('img')
        let musicBoxAristInfo = document.createElement('div')
        let trackName = document.createElement('h4')
        let artistName = document.createElement('span')
        let musicBoxRight = document.createElement('div')
        let likeIcon = document.createElement('img')
        let musicDuration = document.createElement('span')
        let menuBox = document.createElement('div')
        let menuDrop = document.createElement('div')
        let dropBtnLike = document.createElement('button')
        let dropBtnAdd = document.createElement('button')
        let dropBtnListen = document.createElement('button')
        let menuIcon = document.createElement('div')
        for (let i = 1; i <= 3; i++) {
            let dot = document.createElement('span')
            menuIcon.append(dot)
        }
        musicBox.classList.add('main-screen__two-container-music__box')

        musicBoxLeft.classList.add('music__box-left')
        songNum.innerHTML = order++
        songImg.src = tracks[num].song.poster
        songImg.classList.add('music__box-left-track__poster')
        musicBoxAristInfo.classList.add('music__box-artist-info')
        trackName.innerHTML = tracks[num].song.name
        artistName.innerHTML = tracks[num].artist
        musicBoxRight.classList.add('music__box-right')
        if (tracks[num].isFavorite && user.isPremium === undefined) {
            likeIcon.classList.add('active')
            likeIcon.src = '/app/assets/Icons/active-love.svg'
        } else if (tracks[num].isFavorite) {
            likeIcon.classList.add('active')
            likeIcon.src = '/app/assets/Icons/love-active-pr.svg'
        } else {
            likeIcon.classList.remove('active')
            likeIcon.src = '/app/assets/Icons/love.svg'
        }
        musicDuration.classList.add('music__duration')
        musicDuration.innerHTML = tracks[num].song.duration
        menuBox.classList.add('menu__box')
        menuDrop.classList.add('menu__drop')
        tracks[num].isFavorite ? dropBtnLike.innerHTML = 'Dislike' : dropBtnLike.innerHTML = 'Like';
        dropBtnAdd.innerHTML = 'Add to playlist'
        dropBtnListen.innerHTML = 'Listen now'
        menuIcon.classList.add('menu__icon')

        menuDrop.append(dropBtnLike, dropBtnAdd, dropBtnListen)
        menuBox.append(menuDrop, menuIcon)
        musicBoxRight.append(likeIcon, musicDuration, menuBox)
        musicBoxAristInfo.append(trackName, artistName)
        musicBoxLeft.append(songNum, songImg, musicBoxAristInfo)
        musicBox.append(musicBoxLeft, musicBoxRight)
        cont.append(musicBox)

        dropBtnLike.onclick =
            likeIcon.onclick = () => {
                tracks[num].isFavorite ? tracks[num].isFavorite = false : tracks[num].isFavorite = true;
                createSongCol(tracks, cont)
                dataPatch(host, 'tracks', tracks[num].id, tracks[num])
            }
        menuIcon.onclick = () => {
            let menuDrops = document.querySelectorAll('.menu__drop')
            let menuBoxes = document.querySelectorAll('.main-screen__two-container-music__box')
            menuBoxes.forEach(box => {
                menuDrops.forEach(drop => {
                    box.classList.remove('clicked')
                    drop.classList.remove('clicked')
                })
            })
            menuBox.classList.add('clicked')
            menuDrop.classList.add('clicked')
        }
        dropBtnListen.onclick = () => {
            if (audio.classList.contains('pending')) {
                pauseMusic(audio, document.querySelector('.play'))
            } else {
                initMusic(tracks[num], audio)
                playMusic(audio, document.querySelector('.play'))
            }
        }
    }
}
export function createPlayer() {
    const footer = document.createElement('footer')
    footer.classList.add('footer')
    footer.innerHTML = `
    <div class="public-player">
    <div class="public-player-container">
        <div class="public-player__music">
            <h2 class="trackName">Choose a song</h2>
            <span class="artistName">Noname</span>
        </div>
        <div class="public-player-player">
            <div class="public-player-player__actions">
                <img class="playBehavior" src="./assets/Icons/repeat.svg" alt="repeat">
                <img class="nextMusic" src="./assets/Icons/track-next.svg" alt="next">
                <img class="play" src="./assets/Icons/play.svg" class="play" alt="play">
                <img class="prev" src="./assets/Icons/track-prev.svg" alt="previus">
                <img class="shufle" src="./assets/Icons/random.svg" alt="random">
                <div class="menu__box">
                    <div class="menu__icon">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                    <div class="menu__drop">
                        <button class="drpBtnLike">Like</button>
                        <button class="drpBtnAdd">Add to playlist</button>
                        <button class="drpBtnListen">Listen now</button>
                    </div>
                </div>
            </div>
            <div class="public-player-player__progress">
                <span class="currentTime">∞</span>
                <div class="public-player-player__progress__bar">
                    <div class="public-player-player__progress__condition"></div>
                </div>
                <span class="songEnding">∞</span>
            </div>
        </div>
        <div class="empty">
            <audio class="player" preload="metadata"></audio>
        </div>
    </div>
</div>
    `
    document.body.append(footer)
}
function initMusic(track, audio) {
    let curTime = document.querySelector('.currentTime')
    let songEnding = document.querySelector('.songEnding')
    let progress = document.querySelector('.public-player-player__progress__condition')
    let progressBar = document.querySelector('.public-player-player__progress__bar')

    let trackName = document.querySelector('.trackName')
    let artistName = document.querySelector('.artistName')
    let menuIcon = document.querySelector('.menu__icon')
    let menuDrops = document.querySelectorAll('.menu__drop')
    let drpBtnLike = document.querySelector('.drpBtnLike')
    let drpBtnAdd = document.querySelector('.drpBtnAdd')
    let drpBtnListen = document.querySelector('.drpBtnListen')
    track.isFavorite ? drpBtnLike.innerHTML = 'Dislike' : drpBtnLike.innerHTML = 'Like'
    trackName.innerHTML = track.song.name
    artistName.innerHTML = track.artist
    audio.setAttribute('src', track.song.src)
    menuIcon.onclick = () => {
        menuDrops.forEach(drop => {
            drop.classList.add('clicked')
        })
    }
    audio.ontimeupdate = (e) => {
        let { duration, currentTime } = e.srcElement
        progress.style.width = `${(currentTime / duration) * 100}%`
        songEnding.innerHTML = track.song.duration
        curTime.innerHTML = parseToMinute(currentTime)
    }
    progressBar.onclick = (e) => {
        let width = progressBar.clientWidth
        let clicked = e.offsetX
        audio.currentTime = (clicked * audio.duration) / width
    }
    function parseToMinute(time) {
        let fixedTime = (time / 60).toFixed(2).replace('.', ':')
        return fixedTime
    }
}
function playMusic(audio, btn) {
    audio.classList.add('pending')
    btn.src = '/app/assets/Icons/pause.svg'
    audio.play()
    btn.onclick = () => {
        pauseMusic(audio, btn)
    }
}
function pauseMusic(audio, btn) {
    audio.classList.remove('pending')
    btn.src = '/app/assets/Icons/play.svg'
    audio.pause()
    btn.onclick = () => {
        playMusic(audio, btn)
    }
}
