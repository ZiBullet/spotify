export function openSearch(searchField, searchDrop, tracks) {
    searchDrop.style.display = 'flex'
    setTimeout(() => {
        searchDrop.style.opacity = '1'
        searchDrop.style.marginTop = '8px'
    }, 200);
    
    searchField.onkeyup = () => {
        let found = tracks.filter(track => track.song.name.toLowerCase().trim().includes(searchField.value.toLowerCase().trim()) || track.artist.toLowerCase().trim().includes(searchField.value.toLowerCase().trim()))
        found.length > 0 ? reloadSearchDrop(found) : searchDrop.innerHTML = `We could not find such music as ' ${searchField.value} '`
    }
    function reloadSearchDrop(tracks) {
        searchDrop.innerHTML = ''
        for (let track of tracks) {
            let {name, duration} = track.song
    
            let trackRow = document.createElement('div')
            let trackInfo = document.createElement('div')
            let trackName = document.createElement('h2')
            let trackAritist = document.createElement('span')
            let trackDuration = document.createElement('span')
    
            trackRow.classList.add('header-search__drop__music')
            trackInfo.classList.add('header-search__drop-music__creator')
            trackAritist.classList.add('header-search__drop-artist__name')
            trackAritist.innerHTML = track.artist
            trackName.classList.add('header-search__drop-music__name')
            trackName.innerHTML = name
            trackDuration.classList.add('header-search__drop-music__duration')
            trackDuration.innerHTML = duration
    
            trackInfo.append(trackName, trackAritist)
            trackRow.append(trackInfo, trackDuration)
            searchDrop.append(trackRow)
        }
    }
    reloadSearchDrop(tracks)
}
export function closeSearch(searchDrop) {
    searchDrop.style.opacity = '0'
    searchDrop.style.marginTop = '0'
    setTimeout(() => {
        searchDrop.style.display = 'none'
    }, 200);
}